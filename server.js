const express = require('express');
const app = express();

const todos = [
  {
      "title": "Do Interview",
      "completed": true,
      "id": 1,
  },
  {
    "title": "Do Interview Again",
    "completed": true,
    "id": 2,
  },
];

app.set('port', (process.env.PORT || 3000));

app.get('/api/todos', (req, res) => {
    res.setHeader('Cache-Control', 'no-cache');
    res.json((todos));
});

app.put('/api/todos/:id', function (req, res) {
    const markCompletedTodos = todos.map(todo => {
      if(todo.id == req.params.id){
        return Object.assign({},todo,{completed: !todo.completed});
      }
      else{
        return todo;
      }
    });
    res.json(markCompletedTodos);
});

app.listen(app.get('port'), () => {
  console.log(`Find the server at: http://localhost:${app.get('port')}/`); // eslint-disable-line no-console
});
