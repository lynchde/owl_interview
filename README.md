npm run start
````
The server is now running at [localhost:3000](localhost:3000)

Tests to run from Chrome (all return the full list of tasks):

fetch('http://localhost:3000/api/todos/1', {
    method: 'PUT',
});

fetch('http://localhost:3000/api/todos/2', {
    method: 'PUT',
});

fetch('http://localhost:3000/api/todos', {
    method: 'GET',
});